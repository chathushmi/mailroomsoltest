﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MailRoomSolution
{
    static class Logger
    {
        public static void WriteErrorLog(string Message)
        {
            String sLogPath = "C:/dips/logs/CDMLogger.txt";
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(sLogPath, true);
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + ": " + Message);
                sw.Flush();
                sw.Close();
            }
            catch
            {
            }
        }
    }
}
