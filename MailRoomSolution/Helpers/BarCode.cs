﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MailRoomSolution
{
    class BarCode
    {
        public string QRCode { get; set; }
        public int BinNo { get; set; }
        int Status = 102;
        string HOReceivedTime = DateTime.Now.ToString("HH:mm:ss");
        string HOReceivedDate = DateTime.Now.ToString("yyyy-MM-dd");
 
        SqlConnection conn = null;

        public BarCode()
        {
            string serverName;
            string databaseName;
            string connectionString;

            try
            {
                serverName = ConfigurationManager.AppSettings["ServerName"];
                databaseName = ConfigurationManager.AppSettings["DatabaseName"];
                connectionString = "Data Source=" + serverName + ";Initial Catalog=" + databaseName + ";Trusted_Connection=True";

                conn = new SqlConnection(connectionString);
            }
            catch (Exception ex)
            {
                Logger.WriteErrorLog("Error fetching settings from Configuration : " + ex.ToString());
            }
        }

        public string selectData(BarCode b)
        {
            string binNo;

            try
            {
                string sql = "SELECT SortBinID from DOCUMENTTRANSFER WHERE QRCode=@QRCode";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                cmd.Parameters.AddWithValue("@QRCode", b.QRCode);
                
                conn.Open();
                binNo = Convert.ToString(cmd.ExecuteScalar());
                return binNo;

            }
            catch
            {
                MessageBox.Show("Failed to Identify..Try Again!");
                binNo = null;
                return binNo;
            }
            finally
            {
                conn.Close();
            }
        } 

        public bool updateData(BarCode b)
        {
            bool isSuccess = false;

            try
            {
                string sql = "UPDATE DOCUMENTTRANSFER SET HOReceivedTime =@HOReceivedTime,HOReceivedDate =@HOReceivedDate,Status =@Status WHERE QRCode=@QRCode";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@HOReceivedTime", HOReceivedTime);
                cmd.Parameters.AddWithValue("@HOReceivedDate", HOReceivedDate);
                cmd.Parameters.AddWithValue("@Status", Status);
                cmd.Parameters.AddWithValue("@QRCode", b.QRCode);

                conn.Open();
                int rows = cmd.ExecuteNonQuery();

                if (rows > 0)
                 {
                     isSuccess = true;
                 }
                 else
                 {
                     isSuccess = false;
                 }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Failed to Update..Try Again!");
                isSuccess = false;
            }
            finally
            {
                conn.Close();
            }
            return isSuccess;      
        }

        public DataTable Select(BarCode b)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select d.QRCode,d.SortBinID,l.LocationName from DOCUMENTTRANSFER d JOIN LOCATIONS l ON d.DestID = l.LocationID WHERE d.QRCode=@QRCode";
                SqlCommand cmd = new SqlCommand(sql, conn);
                cmd.Parameters.AddWithValue("@QRCode", b.QRCode);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                conn.Open();
                adapter.Fill(dt);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error while Populate data");
            }
            finally
            {
                conn.Close();
            }
            return dt;
        }
    }
}
