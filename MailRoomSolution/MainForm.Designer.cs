﻿namespace MailRoomSolution
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelMailRoom = new System.Windows.Forms.Label();
            this.buttonUnLoading = new System.Windows.Forms.Button();
            this.buttonLoading = new System.Windows.Forms.Button();
            this.buttonMinimize = new System.Windows.Forms.Button();
            this.buttonCloseApp = new System.Windows.Forms.Button();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageLoading = new System.Windows.Forms.TabPage();
            this.tabPageUnLoading = new System.Windows.Forms.TabPage();
            this.panel1.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel1.BackgroundImage")));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelMailRoom);
            this.panel1.Controls.Add(this.buttonUnLoading);
            this.panel1.Controls.Add(this.buttonLoading);
            this.panel1.Controls.Add(this.buttonMinimize);
            this.panel1.Controls.Add(this.buttonCloseApp);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1333, 64);
            this.panel1.TabIndex = 0;
            // 
            // labelMailRoom
            // 
            this.labelMailRoom.AutoSize = true;
            this.labelMailRoom.BackColor = System.Drawing.Color.Transparent;
            this.labelMailRoom.Font = new System.Drawing.Font("Microsoft Sans Serif", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMailRoom.Location = new System.Drawing.Point(56, 18);
            this.labelMailRoom.Name = "labelMailRoom";
            this.labelMailRoom.Size = new System.Drawing.Size(159, 32);
            this.labelMailRoom.TabIndex = 5;
            this.labelMailRoom.Text = "Mail Room";
            // 
            // buttonUnLoading
            // 
            this.buttonUnLoading.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonUnLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonUnLoading.Location = new System.Drawing.Point(481, 2);
            this.buttonUnLoading.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonUnLoading.Name = "buttonUnLoading";
            this.buttonUnLoading.Size = new System.Drawing.Size(188, 54);
            this.buttonUnLoading.TabIndex = 4;
            this.buttonUnLoading.Text = "Dispatching";
            this.buttonUnLoading.UseVisualStyleBackColor = false;
            this.buttonUnLoading.Click += new System.EventHandler(this.buttonUnLoading_Click);
            // 
            // buttonLoading
            // 
            this.buttonLoading.BackColor = System.Drawing.Color.LightSeaGreen;
            this.buttonLoading.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonLoading.Location = new System.Drawing.Point(285, 2);
            this.buttonLoading.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonLoading.Name = "buttonLoading";
            this.buttonLoading.Size = new System.Drawing.Size(188, 54);
            this.buttonLoading.TabIndex = 3;
            this.buttonLoading.Text = "Sorting";
            this.buttonLoading.UseVisualStyleBackColor = false;
            this.buttonLoading.Click += new System.EventHandler(this.buttonLoading_Click);
            // 
            // buttonMinimize
            // 
            this.buttonMinimize.BackColor = System.Drawing.Color.Lime;
            this.buttonMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonMinimize.Location = new System.Drawing.Point(1183, 2);
            this.buttonMinimize.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonMinimize.Name = "buttonMinimize";
            this.buttonMinimize.Size = new System.Drawing.Size(53, 54);
            this.buttonMinimize.TabIndex = 2;
            this.buttonMinimize.Text = "-";
            this.buttonMinimize.UseVisualStyleBackColor = false;
            this.buttonMinimize.Click += new System.EventHandler(this.buttonMinimize_Click);
            // 
            // buttonCloseApp
            // 
            this.buttonCloseApp.BackColor = System.Drawing.Color.Red;
            this.buttonCloseApp.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCloseApp.Location = new System.Drawing.Point(1244, 2);
            this.buttonCloseApp.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonCloseApp.Name = "buttonCloseApp";
            this.buttonCloseApp.Size = new System.Drawing.Size(53, 54);
            this.buttonCloseApp.TabIndex = 1;
            this.buttonCloseApp.Text = "X";
            this.buttonCloseApp.UseVisualStyleBackColor = false;
            this.buttonCloseApp.Click += new System.EventHandler(this.buttonCloseApp_Click);
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageLoading);
            this.tabControl.Controls.Add(this.tabPageUnLoading);
            this.tabControl.Location = new System.Drawing.Point(-11, 12);
            this.tabControl.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1333, 738);
            this.tabControl.TabIndex = 1;
            // 
            // tabPageLoading
            // 
            this.tabPageLoading.BackgroundImage = global::MailRoomSolution.Properties.Resources.mailRoombg;
            this.tabPageLoading.Location = new System.Drawing.Point(4, 25);
            this.tabPageLoading.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageLoading.Name = "tabPageLoading";
            this.tabPageLoading.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageLoading.Size = new System.Drawing.Size(1325, 709);
            this.tabPageLoading.TabIndex = 0;
            this.tabPageLoading.Text = "tabPageLoading";
            this.tabPageLoading.UseVisualStyleBackColor = true;
            // 
            // tabPageUnLoading
            // 
            this.tabPageUnLoading.BackgroundImage = global::MailRoomSolution.Properties.Resources.mailRoombg;
            this.tabPageUnLoading.Location = new System.Drawing.Point(4, 25);
            this.tabPageUnLoading.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageUnLoading.Name = "tabPageUnLoading";
            this.tabPageUnLoading.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabPageUnLoading.Size = new System.Drawing.Size(1325, 709);
            this.tabPageUnLoading.TabIndex = 1;
            this.tabPageUnLoading.Text = "tabPageUnLoading";
            this.tabPageUnLoading.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1312, 690);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MailRoom";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonCloseApp;
        private System.Windows.Forms.Button buttonMinimize;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageLoading;
        private System.Windows.Forms.TabPage tabPageUnLoading;
        private System.Windows.Forms.Button buttonUnLoading;
        private System.Windows.Forms.Button buttonLoading;
        private System.Windows.Forms.Label labelMailRoom;
    }
}