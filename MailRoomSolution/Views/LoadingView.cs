﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MailRoomSolution.Views
{

    public partial class LoadingView : UserControl
    {
        BarCode b = new BarCode();

        public LoadingView()
        {
            InitializeComponent();
            txtCode.Focus();
        }

        private void txtCode_TextChanged(object sender, EventArgs e)
        {

            if(txtCode.Text.Length == 12)
            {
                
                b.QRCode = txtCode.Text;
                lblBin.Text = b.selectData(b);
                
                bool isSuccess = b.updateData(b);
                if (isSuccess == false)
                {
                    MessageBox.Show("Unavailable Entry!", "title", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                DataTable dt = b.Select(b);
                dataGridView1.DataSource = dt;

                txtCode.SelectAll();
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            labelNotReady.Text = "- - Ready to Scan - -";
            labelNotReady.ForeColor = Color.Green;
            txtCode.Focus();
        }

        public void ResetProperties()
        {
            txtCode.Text = "";
            lblBin.Text = "";

            b.QRCode = txtCode.Text;
            DataTable dt = b.Select(b);
            dataGridView1.DataSource = dt;
            dataGridView1.Refresh();

            labelNotReady.Text = "- - Not Ready to Scan - -";
            labelNotReady.ForeColor = Color.Red;
            //txtCode.Focus();
        }
    }
}
