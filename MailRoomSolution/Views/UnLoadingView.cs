﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Media;
using System.IO;
using MailRoomSolution.Properties;

namespace MailRoomSolution.Views
{
    public partial class UnLoadingView : UserControl
    {
        DataTable t = new DataTable();
        int count;

        public UnLoadingView()
        {
            InitializeComponent();
            UnLoadingView_Load();
            showdatagridview();
        }
        SqlConnection con;
        SqlCommand cmd;
        SqlDataReader dr;
        private void UnLoadingView_Load()
        {
            try
            {
                con = new SqlConnection(@"Data Source=TI-VM-CBC-COM;Initial Catalog=MailRoom;TRUSTED_CONNECTION = TRUE");
                //con = new SqlConnection(@"Data Source=TI-NB-163\SQLEXPRESS;Initial Catalog=MailRoom;User ID=cdmuser;Password=Qw#4rt67;TRUSTED_CONNECTION = TRUE");
                cmd = new SqlCommand();
                con.Open();
                cmd.Connection = con;
                cmd.CommandText = "SELECT * FROM LOCATIONS";
                dr = cmd.ExecuteReader();
                this.comboBoxBranchName.DataSource = null;
                this.comboBoxBranchName.Items.Clear();
                while (dr.Read())
                {
                    comboBoxBranchName.Items.Add(dr["LocationName"].ToString());
                    count = 0;
                }
                con.Close();
                textBoxQR.Focus();
            }
            catch (Exception ex)
            {
                con.Close();
            }
            
        }
        
        private void textBoxQR_TextChanged(object sender, EventArgs e)
        {
            if (textBoxQR.TextLength == 12)
            {
                try
                {
                    con = new SqlConnection(@"Data Source=TI-VM-CBC-COM;Initial Catalog=MailRoom;TRUSTED_CONNECTION = TRUE;MultipleActiveResultSets=true");
                    //con = new SqlConnection(@"Data Source=TI-NB-163\SQLEXPRESS;Initial Catalog=MailRoom;User ID=cdmuser;Password=Qw#4rt67;TRUSTED_CONNECTION = TRUE");
                    cmd = new SqlCommand();
                    con.Open();
                    cmd.Connection = con;
                    cmd.CommandText = "SELECT DOCUMENTTRANSFER.QRCode, LOCATIONS.LocationName " +
                        "FROM DOCUMENTTRANSFER INNER JOIN LOCATIONS " +
                        "ON DOCUMENTTRANSFER.DestID=LOCATIONS.LocationID " +
                        "WHERE DOCUMENTTRANSFER.QRCode='"+ textBoxQR .Text+ "';";
                    dr = cmd.ExecuteReader();
                    this.labelRefValue.Text = null;
                    this.labelRefValue.Text = string.Empty;
                    if (dr.Read())  
                    {
                        labelRefValue.Text = dr["QRCode"].ToString();
                        labelToBranchValue.Text = dr["LocationName"].ToString();

                        if (comboBoxBranchName.Text != labelToBranchValue.Text)
                        {
                            ErrorBeep();
                            labelRefValue.BackColor = Color.Red;
                            labelToBranchValue.BackColor = Color.Red;
                        }
                        else
                        {
                            SuccessBeep();
                            labelRefValue.BackColor = Color.Green;
                            labelToBranchValue.BackColor = Color.Green;
                            count++;
                            t.Rows.Add(count, labelRefValue.Text);
                            labelCount.Text = count.ToString();
                            con.Close();
                            ChangeStatus();

                        }
                    }
                    else
                    {
                        ErrorBeep();
                        labelRefValue.Text = textBoxQR.Text;
                        labelToBranchValue.Text = string.Empty;
                        labelRefValue.BackColor = Color.Yellow;
                        labelToBranchValue.BackColor = Color.Yellow;
                        MessageBox.Show("Cannot find document details in the system");                        
                    }
                    con.Close();
                    textBoxQR.SelectAll();                    
                }
                catch (Exception ex)
                {
                    con.Close();
                }
            }           

        }
        public void ChangeStatus()
        {
            try
            {
                con = new SqlConnection(@"Data Source=TI-VM-CBC-COM;Initial Catalog=MailRoom;TRUSTED_CONNECTION = TRUE;MultipleActiveResultSets=true");
                cmd = new SqlCommand();
                con.Open();
                cmd.Connection = con;
                cmd.CommandText = "UPDATE DOCUMENTTRANSFER SET Status =" + 104 + " WHERE QRCode=" + labelRefValue.Text;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                con.Close();
            }
        }

        SoundPlayer soundPlayer;
        public void SuccessBeep()
        {
            //string voiceCommand = Path.Combine(@"D:\TIQRICDM\cdm\Inputs\ButtonClick\Success-alert.wav");
            using (soundPlayer = new System.Media.SoundPlayer(Resources.Success_alert))
            {
                soundPlayer.Play();
            }
        }
        public void ErrorBeep()
        {
            //string voiceCommand = Path.Combine(@"D:\TIQRICDM\cdm\Inputs\ButtonClick\Wrong-alert.wav");
            using (soundPlayer = new System.Media.SoundPlayer(Resources.Wrong_alert))
            {
                soundPlayer.Play();
            }
        }

        private void buttonStartScan_Click(object sender, EventArgs e)
        {
            if (comboBoxBranchName.Text != " -- Select Branch -- ")
            {
                labelReadyAlert.BringToFront();
                textBoxQR.Focus();
            }
            else
            {
                labelReadyAlert.SendToBack();
            }            
        }
        public void ResetProperties()
        {
            labelReadyAlert.SendToBack();
            labelToBranchValue.BackColor = Color.Transparent;
            labelToBranchValue.Text = "--";
            labelRefValue.BackColor = Color.Transparent;
            labelRefValue.Text = "--";
        }

        public void showdatagridview()
        {
            t.Columns.Add("ITEM", typeof(int));
            t.Columns.Add("QR", typeof(string));
            dataGridViewCount.DataSource = t;
        }

        private void comboBoxBranchName_SelectedIndexChanged(object sender, EventArgs e)
        {
            count = 0;
            labelCount.Text = count.ToString();
            if (t.Rows.Count >= 2)
            {
                t.Rows.Clear();
            }
        }

        private void labelToBranchValue_Click(object sender, EventArgs e)
        {

        }
    }
}
