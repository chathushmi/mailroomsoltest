﻿namespace MailRoomSolution.Views
{
    partial class UnLoadingView
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UnLoadingView));
            this.comboBoxBranchName = new System.Windows.Forms.ComboBox();
            this.lOCATIONSBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.mailRoomDataSet = new MailRoomSolution.MailRoomDataSet();
            this.lOCATIONSTableAdapter = new MailRoomSolution.MailRoomDataSetTableAdapters.LOCATIONSTableAdapter();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.labelToBranchValue = new System.Windows.Forms.Label();
            this.labelRefValue = new System.Windows.Forms.Label();
            this.labelToBranch = new System.Windows.Forms.Label();
            this.labelRef = new System.Windows.Forms.Label();
            this.textBoxQR = new System.Windows.Forms.TextBox();
            this.buttonStartScan = new System.Windows.Forms.Button();
            this.labelReadyAlert = new System.Windows.Forms.Label();
            this.labelNotReadyAlert = new System.Windows.Forms.Label();
            this.dataGridViewCount = new System.Windows.Forms.DataGridView();
            this.ITEM = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.QR = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelCount = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.lOCATIONSBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailRoomDataSet)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCount)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // comboBoxBranchName
            // 
            this.comboBoxBranchName.BackColor = System.Drawing.Color.White;
            this.comboBoxBranchName.DataSource = this.lOCATIONSBindingSource;
            this.comboBoxBranchName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxBranchName.FormattingEnabled = true;
            this.comboBoxBranchName.Location = new System.Drawing.Point(149, 215);
            this.comboBoxBranchName.Margin = new System.Windows.Forms.Padding(4);
            this.comboBoxBranchName.Name = "comboBoxBranchName";
            this.comboBoxBranchName.Size = new System.Drawing.Size(504, 38);
            this.comboBoxBranchName.TabIndex = 1;
            this.comboBoxBranchName.SelectedIndexChanged += new System.EventHandler(this.comboBoxBranchName_SelectedIndexChanged);
            // 
            // lOCATIONSBindingSource
            // 
            this.lOCATIONSBindingSource.DataMember = "LOCATIONS";
            this.lOCATIONSBindingSource.DataSource = this.mailRoomDataSet;
            // 
            // mailRoomDataSet
            // 
            this.mailRoomDataSet.DataSetName = "MailRoomDataSet";
            this.mailRoomDataSet.Namespace = "http://tempuri.org/MailRoomDataSet.xsd";
            this.mailRoomDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // lOCATIONSTableAdapter
            // 
            this.lOCATIONSTableAdapter.ClearBeforeFill = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.labelToBranchValue, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelRefValue, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.labelToBranch, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.labelRef, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(145, 308);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(4);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.73494F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 66.26506F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(656, 188);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // labelToBranchValue
            // 
            this.labelToBranchValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelToBranchValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToBranchValue.Location = new System.Drawing.Point(332, 85);
            this.labelToBranchValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelToBranchValue.Name = "labelToBranchValue";
            this.labelToBranchValue.Size = new System.Drawing.Size(320, 80);
            this.labelToBranchValue.TabIndex = 4;
            this.labelToBranchValue.Text = "--";
            this.labelToBranchValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelToBranchValue.Click += new System.EventHandler(this.labelToBranchValue_Click);
            // 
            // labelRefValue
            // 
            this.labelRefValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRefValue.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRefValue.Location = new System.Drawing.Point(4, 85);
            this.labelRefValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRefValue.Name = "labelRefValue";
            this.labelRefValue.Size = new System.Drawing.Size(320, 80);
            this.labelRefValue.TabIndex = 4;
            this.labelRefValue.Text = "--";
            this.labelRefValue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelToBranch
            // 
            this.labelToBranch.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelToBranch.AutoSize = true;
            this.labelToBranch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelToBranch.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToBranch.Location = new System.Drawing.Point(332, 15);
            this.labelToBranch.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelToBranch.Name = "labelToBranch";
            this.labelToBranch.Size = new System.Drawing.Size(320, 33);
            this.labelToBranch.TabIndex = 4;
            this.labelToBranch.Text = "Destination Branch";
            this.labelToBranch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelRef
            // 
            this.labelRef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelRef.AutoSize = true;
            this.labelRef.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.labelRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelRef.Location = new System.Drawing.Point(4, 15);
            this.labelRef.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelRef.Name = "labelRef";
            this.labelRef.Size = new System.Drawing.Size(320, 33);
            this.labelRef.TabIndex = 3;
            this.labelRef.Text = "Reference";
            this.labelRef.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxQR
            // 
            this.textBoxQR.Location = new System.Drawing.Point(149, 230);
            this.textBoxQR.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxQR.Name = "textBoxQR";
            this.textBoxQR.Size = new System.Drawing.Size(12, 22);
            this.textBoxQR.TabIndex = 3;
            this.textBoxQR.TextChanged += new System.EventHandler(this.textBoxQR_TextChanged);
            // 
            // buttonStartScan
            // 
            this.buttonStartScan.BackColor = System.Drawing.Color.SpringGreen;
            this.buttonStartScan.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonStartScan.Location = new System.Drawing.Point(659, 215);
            this.buttonStartScan.Margin = new System.Windows.Forms.Padding(4);
            this.buttonStartScan.Name = "buttonStartScan";
            this.buttonStartScan.Size = new System.Drawing.Size(149, 47);
            this.buttonStartScan.TabIndex = 5;
            this.buttonStartScan.Text = "Start Scan";
            this.buttonStartScan.UseVisualStyleBackColor = false;
            this.buttonStartScan.Click += new System.EventHandler(this.buttonStartScan_Click);
            // 
            // labelReadyAlert
            // 
            this.labelReadyAlert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelReadyAlert.BackColor = System.Drawing.Color.Transparent;
            this.labelReadyAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelReadyAlert.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.labelReadyAlert.Location = new System.Drawing.Point(145, 260);
            this.labelReadyAlert.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelReadyAlert.Name = "labelReadyAlert";
            this.labelReadyAlert.Size = new System.Drawing.Size(320, 41);
            this.labelReadyAlert.TabIndex = 6;
            this.labelReadyAlert.Text = "Ready To Scan";
            this.labelReadyAlert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labelNotReadyAlert
            // 
            this.labelNotReadyAlert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelNotReadyAlert.BackColor = System.Drawing.Color.Transparent;
            this.labelNotReadyAlert.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNotReadyAlert.ForeColor = System.Drawing.Color.Red;
            this.labelNotReadyAlert.Location = new System.Drawing.Point(149, 261);
            this.labelNotReadyAlert.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.labelNotReadyAlert.Name = "labelNotReadyAlert";
            this.labelNotReadyAlert.Size = new System.Drawing.Size(320, 38);
            this.labelNotReadyAlert.TabIndex = 7;
            this.labelNotReadyAlert.Text = "Not Ready To Scan";
            this.labelNotReadyAlert.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dataGridViewCount
            // 
            this.dataGridViewCount.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridViewCount.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridViewCount.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridViewCount.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewCount.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ITEM,
            this.QR});
            this.dataGridViewCount.Location = new System.Drawing.Point(866, 289);
            this.dataGridViewCount.Name = "dataGridViewCount";
            this.dataGridViewCount.RowTemplate.Height = 24;
            this.dataGridViewCount.Size = new System.Drawing.Size(389, 298);
            this.dataGridViewCount.TabIndex = 9;
            // 
            // ITEM
            // 
            this.ITEM.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.ITEM.DataPropertyName = "ITEM";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ITEM.DefaultCellStyle = dataGridViewCellStyle2;
            this.ITEM.FillWeight = 60F;
            this.ITEM.HeaderText = "ITEM #";
            this.ITEM.Name = "ITEM";
            // 
            // QR
            // 
            this.QR.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.QR.DataPropertyName = "QR";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.QR.DefaultCellStyle = dataGridViewCellStyle3;
            this.QR.HeaderText = "QR CODE";
            this.QR.Name = "QR";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.labelCount);
            this.panel1.Location = new System.Drawing.Point(938, 106);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(238, 155);
            this.panel1.TabIndex = 11;
            // 
            // labelCount
            // 
            this.labelCount.AutoSize = true;
            this.labelCount.BackColor = System.Drawing.Color.Transparent;
            this.labelCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 72F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCount.Location = new System.Drawing.Point(62, 10);
            this.labelCount.Name = "labelCount";
            this.labelCount.Size = new System.Drawing.Size(124, 135);
            this.labelCount.TabIndex = 12;
            this.labelCount.Text = "0";
            // 
            // UnLoadingView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dataGridViewCount);
            this.Controls.Add(this.labelNotReadyAlert);
            this.Controls.Add(this.labelReadyAlert);
            this.Controls.Add(this.buttonStartScan);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.comboBoxBranchName);
            this.Controls.Add(this.textBoxQR);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "UnLoadingView";
            this.Size = new System.Drawing.Size(1301, 658);
            ((System.ComponentModel.ISupportInitialize)(this.lOCATIONSBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mailRoomDataSet)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewCount)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox comboBoxBranchName;
        private System.Windows.Forms.BindingSource lOCATIONSBindingSource;
        private MailRoomDataSet mailRoomDataSet;
        private MailRoomDataSetTableAdapters.LOCATIONSTableAdapter lOCATIONSTableAdapter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label labelToBranchValue;
        private System.Windows.Forms.Label labelRefValue;
        private System.Windows.Forms.Label labelToBranch;
        private System.Windows.Forms.Label labelRef;
        private System.Windows.Forms.TextBox textBoxQR;
        private System.Windows.Forms.Button buttonStartScan;
        private System.Windows.Forms.Label labelReadyAlert;
        private System.Windows.Forms.Label labelNotReadyAlert;
        private System.Windows.Forms.DataGridView dataGridViewCount;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn ITEM;
        private System.Windows.Forms.DataGridViewTextBoxColumn QR;
    }
}
