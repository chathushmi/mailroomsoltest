﻿using MailRoomSolution.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MailRoomSolution
{
    public partial class MainForm : Form
    {
        private LoadingView loadingView;
        private UnLoadingView unLoadingView;
        public MainForm()
        {
            InitializeComponent();

            //loadingView
            loadingView = new LoadingView();
            loadingView.Dock = DockStyle.Fill;
            //loadingView.BackgroundImage = Resources.bg_image_new;
            tabPageLoading.Controls.Add(loadingView);

            //loadingView
            unLoadingView = new UnLoadingView();
            unLoadingView.Dock = DockStyle.Fill;
            //loadingView.BackgroundImage = Resources.bg_image_new;
            tabPageUnLoading.Controls.Add(unLoadingView);

            //this.barCodeDetailsTableAdapter.Fill(this.mailRoomDataSet.BarCodeDetails);
        }

        private void buttonCloseApp_Click(object sender, EventArgs e)
        {
            Application.Exit();
            Application.ExitThread();
        }

        private void buttonMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void buttonLoading_Click(object sender, EventArgs e)
        {
            loadingView.ResetProperties();
            tabControl.SelectedTab = tabPageLoading;
        }

        private void buttonUnLoading_Click(object sender, EventArgs e)
        {
            unLoadingView.ResetProperties();
            tabControl.SelectedTab = tabPageUnLoading;
        }
    }
}
