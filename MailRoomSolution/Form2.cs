﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MailRoomSolution
{
    public partial class Form2 : Form
    {
        BarCode b = new BarCode();

        public Form2()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (bCodeTxt.Text != "")
            {
                binNoTxt.Text = "";
                b.QRCode = bCodeTxt.Text;
                string bin = b.selectData(b);
                binNoTxt.Text = bin;
                bool success = b.updateData(b);
                if(success == true)
                {
                    tmrclear.Enabled = true;
                }
                else if(success == false)
                {
                    MessageBox.Show("Update unsuccessful.. Plese check again.");
                    tmrclear.Enabled = true;
                }
                //tmrclear.Enabled = true;
            }
        }

        private void tmrclear_Tick(object sender, EventArgs e)
        {
            binNoTxt.Text = "";
            bCodeTxt.Text = "";
            tmrclear.Enabled = false;
        }
    }
}
